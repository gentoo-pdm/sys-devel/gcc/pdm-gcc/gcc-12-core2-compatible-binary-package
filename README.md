# gcc-12 core2 compatible binary package



## Where is this binary package coming from?

It's a gentoo binary package created with the command:
emerge -B sys-devel/gcc:12

This command created the /var/cache/binpkgs/sys-devel/gcc-12.2.1_p20221203.tbz2 file uploaded here.

## Why is this binary package available here?

Binary package has been created on skylate host with -march=core2 -mtune=core2 with USE="cxx fortran graphite jit lto multilib nptl openmp pgo pie sanitize vtv zstd -ada -cet -custom-cflags -d -debug -default-stack-clash-protection -default-znow -doc -fixed-point -go -hardened -ieee-long-double -libssp -nls -objc -objc++ -objc-gc -pch -ssp -systemtap -test -valgrind -vanilla"

It has been made available here for potential testers to verify that such a package runs fine on core2, albeit libgccjit generates packaging source host level instructions, e.g. skylake instead of core2.
